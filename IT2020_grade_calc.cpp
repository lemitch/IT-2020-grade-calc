#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>

using namespace std;

int main() {
	ifstream inFile;
	string line;
	bool x = true;
	double total, current = 0;

	inFile.open("data.txt");

	while (inFile >> line) {
		if (x) {
			cout << line << endl;
			current += stod(line);

			x = false;
		} else {
			cout << line << endl;
			if (line == "-")
				line = "0";
			total += stod(line);

			x = true;
		}
	}
	cout << "Without report #3: " << current/total;
	cout << "\nWith report #3: " << (current+50)/total << endl;
	cout << "\nWith all assignments: " << (current+50+75+15+15)/total << endl;

	return 0;
}